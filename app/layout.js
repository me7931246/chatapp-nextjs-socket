import { Inter } from 'next/font/google'

// import Home from './Components/client'
const inter = Inter({ subsets: ['latin'] })

export const metadata = {
  title: 'Create Next App',
  description: 'Generated by create next app',
}

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body >
        {children}
        {/* < Home /> */}
      </body>
    </html>
  )
}
