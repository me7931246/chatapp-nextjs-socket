import React, { useEffect, useState } from 'react';
import io from 'socket.io-client';
import styling from "../styles/chatapp.module.css"
import { set } from 'mongoose';

const username = "Chat1"

export default function Reciever() {

    const [socket, setSocket] = useState(undefined);
    const [inbox, setInbox] = useState([])
    const [messages, setMessages] = useState([]);

    const handleSendMessage = () => {
        const user = {
            userid: '',
            uname: username,
        }
        socket.emit("message", messages);
    }




    useEffect(() => {
        const socket = io("http://localhost:3001")
        socket.on('message', (message) => {
            setInbox([...inbox, message]);
        })
        setSocket(socket);
    }, [])





    return (
        <div className={styling.container}>
            <div className={styling.inputContainer}>

            </div>

            <div className={styling.inputContainer}>
                <input onChange={(e) => {
                    setMessages(e.target.value)
                }} type='text' name='message' className={styling.input} placeholder='Enter Message..'></input>
                <button className={styling.button} onClick={handleSendMessage}>Send</button>
            </div>

            <div>
                <ul className={styling.messageList} >
                    {inbox.map((message, index) => (
                        <li key={index}>
                            <strong className={styling.message}><b>{username} </b>: {message}</strong>
                        </li>

                    ))}
                </ul>
            </div>


        </div>
    )
}

