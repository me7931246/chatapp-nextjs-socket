
import React, { useEffect, useState } from 'react';
import io from 'socket.io-client';
import styling from "../styles/chatapp.module.css"

const socket = io('http://localhost:3000');
const userid = "Chat ";

const Home = () => {
    const [messages, setMessages] = useState([]);
    const [inputText, setInputText] = useState('');
    const [receiver, setReceiver] = useState('');

    useEffect(() => {
        // Listen for incoming messages
        socket.on('message', (data) => {
            setMessages((prevMessages) => [...prevMessages, data]);
        });

        // Cleanup function
        return () => {
            socket.off('message');
        };
    }, []);






    const sendMessage = () => {
        const message = {
            // we have to select sender id which is  logged in id weather user ,Broker or admin from database then we have to pass
            sender: userid,
            receiver,
            text: inputText,
        };

        // Emit the message to the server
        socket.emit('message', message);

        setMessages((prevMessages) => [...prevMessages, message]);
        setInputText('');
    };

    // const recieveMessage=()=>{
    //     const rcvMessage={
    //         username:' select from databse sender username',
    //         message:'select message from sender collections with specific id',
    //     }
    // }






    return (
        <div className={styling.body}>
            <div className={styling.container}>
                <h1 className={styling.title}>Chat App</h1>

                <div className={styling.inputContainer}>

                    <input className={styling.input}
                        type="text"
                        value={inputText}
                        placeholder="Message"
                        onChange={(e) => setInputText(e.target.value)}
                    />
                    <button className={styling.button} onClick={sendMessage}>Send</button>

                </div>

                <ul className={styling.messageList}>
                    {messages.map((message, index) => (
                        <li key={index}>
                            <strong className={styling.message}>{message.sender}:</strong> {message.text}
                        </li>
                    ))}
                </ul>
            </div>


            {/* Reciever backgroubd */}
            {/* <div className={style.container}>
                <div className={style.inputContainer}>
                    <input className={style.input}
                        type="text"
                        value={receiver}
                        placeholder="Message "
                        onChange={(e) => setReceiver(fetch data from database )}
                    />
                   
                </div>
                <ul className={style.messageList}>
                    {messages.map((recievemessage, index) => (
                        <li key={index}>
                            <h4 className={style.message}>{recievemessage.message}:</h4> {message.text}
                        </li>
                    ))}
                </ul>
            </div> */}
        </div>
    );
};

export default Home;